package tainting.intention;

import com.intellij.codeInsight.intention.QuickFixFactory;
import com.intellij.codeInsight.intention.impl.BaseIntentionAction;
import com.intellij.codeInspection.LocalQuickFixAndIntentionActionOnPsiElement;
import com.intellij.openapi.editor.Editor;
import com.intellij.openapi.project.Project;
import com.intellij.psi.*;
import com.intellij.psi.util.PsiTreeUtil;
import com.intellij.util.IncorrectOperationException;
import org.jetbrains.annotations.Nls;
import org.jetbrains.annotations.NotNull;

/**
 * This represents the general tainting flow fix. New method is created and properly annotated, problematic expression
 * is wrapped inside the call. Developer is responsible for correctness of validation function.
 */
public class TaintingExpressionFixIntention extends BaseIntentionAction {

    private PsiExpression originalExpression;
    private static int methodNr = 1;  // TODO: if left this way, should be service (persistent)

    public TaintingExpressionFixIntention(PsiExpression originalExpression) {
        this.originalExpression = originalExpression;
    }

    @NotNull
    @Override
    public String getText() {
        return "Create sanitizer function";
    }

    @Nls
    @NotNull
    @Override
    public String getFamilyName() {
        return "Tainting fix";
    }

    @Override
    public boolean isAvailable(@NotNull Project project, Editor editor, PsiFile file) {
        return true;
    }

    @Override
    public void invoke(@NotNull Project project, Editor editor, PsiFile file) throws IncorrectOperationException {
        // Since startInWriteAction = true, no write command should be needed...
        // Add new method to class and wrap problematic element inside the call. add supress annot to new method

        PsiClass psiClass = PsiTreeUtil.getParentOfType(originalExpression, PsiClass.class);

        // generate sanitizer method name. TODO: maybe can use autosuggests from IntelliJ somehow?
        String validateMethodName = "validate_" + methodNr;
        while (psiClass.findMethodsByName(validateMethodName, false).length > 0) {
            methodNr++;
            validateMethodName = "validate_" + methodNr;
        }

        // create new method
        PsiElementFactory factory = JavaPsiFacade.getInstance(project).getElementFactory();
        PsiMethod validatingMethod = factory.createMethod(validateMethodName, originalExpression.getType());
        PsiParameter parameter = factory.createParameter("input", originalExpression.getType());
        validatingMethod.getParameterList().add(parameter);

        // add sanitizer annotations
        validatingMethod.getModifierList().addAnnotation("Untainted");
        validatingMethod.getModifierList().addAnnotation("SuppressWarnings(\"tainting\")");

        // add return
        PsiReturnStatement returnStatement = (PsiReturnStatement) factory.createStatementFromText("return input;", null);
        validatingMethod.getBody().add(returnStatement);

        // write method to file
//        psiClass.add(validatingMethod);
        LocalQuickFixAndIntentionActionOnPsiElement addMethodFix = QuickFixFactory.getInstance().createAddMethodFix(validatingMethod, psiClass);
        addMethodFix.invoke(project, editor, file);

        // wrap original expression inside new method call
        String validateCallTemplate = validateMethodName + "(x)";
        PsiMethodCallExpression validateCall =
                (PsiMethodCallExpression) factory.createExpressionFromText(validateCallTemplate, null);
        validateCall.getArgumentList().getExpressions()[0].replace(originalExpression);
        originalExpression.replace(validateCall);

        // TODO: Other option would be to reuse ExtractMethod refactor action.
        // TODO: 1. execute inside transaction?? 2. Add @Untainted @SuppressWarnings("tainting")
//        ExtractMethodAction extractMethodAction = new ExtractMethodAction();
//        AnActionEvent anActionEvent = AnActionEvent.createFromDataContext(
//                ActionPlaces.UNKNOWN, extractMethodAction.getTemplatePresentation(),
//                DataManager.getInstance().getDataContext(editor.getComponent())
//        );
//        extractMethodAction.actionPerformed(anActionEvent);
    }
}
