package tainting.intention;

import com.intellij.codeInsight.intention.impl.BaseIntentionAction;
import com.intellij.openapi.editor.Editor;
import com.intellij.openapi.project.Project;
import com.intellij.psi.*;
import com.intellij.util.IncorrectOperationException;
import org.jetbrains.annotations.Nls;
import org.jetbrains.annotations.NotNull;

/**
 * Implements usecase where SQL query string is not directly inside Statement.executeQuery() but a separate variable
 * is used instead.
 * Converts: String query = "SELECT * FROM  User where userId='" + user + "'";
 *           ResultSet res = st.executeQuery(query);
 *       To: ResultSet res = st.executeQuery("SELECT * FROM  User where userId='" + user + "'");
 *
 * After that invokes the statment to prepared statement conversion.
 */
public class StatToPrepStatViaVar extends BaseIntentionAction {
    private PsiExpression originalExpression;

    public StatToPrepStatViaVar(PsiExpression originalExpression) {
        this.originalExpression = originalExpression;
    }

    @NotNull
    @Override
    public String getText() {
        return "Try to change Statement to PreparedStatement";
    }

    @Nls(capitalization = Nls.Capitalization.Sentence)
    @NotNull
    @Override
    public String getFamilyName() {
        return "Tainting fix";
    }

    @Override
    public boolean isAvailable(@NotNull Project project, Editor editor, PsiFile file) {
        if (originalExpression instanceof PsiReferenceExpression) {
            PsiElement varTmp = ((PsiReferenceExpression) originalExpression).resolve();
            if (varTmp instanceof PsiLocalVariable) {
                PsiExpression initializer = ((PsiLocalVariable) varTmp).getInitializer();
                return initializer instanceof PsiPolyadicExpression;
            }
        }
        return false;
    }

    @Override
    public void invoke(@NotNull Project project, Editor editor, PsiFile file) throws IncorrectOperationException {
        // we should have smth like:
        //      String query = " ... "
        //      Statement.executeQuery(query)
        if (originalExpression instanceof PsiReferenceExpression) {
            PsiElement varTmp = ((PsiReferenceExpression) originalExpression).resolve();
            if (varTmp instanceof PsiLocalVariable) {
                PsiExpression initializer = ((PsiLocalVariable) varTmp).getInitializer();
                if (initializer instanceof PsiPolyadicExpression) {
                    // fix will be simply to copy the original expression into executeQuery and then call the other fix
                    originalExpression.replace(initializer);
                    // now we should have Statement.executeQuery("...")
                    new StatementToPreparedStatementIntention().invoke(project, editor, file);
                }
            }
        }
    }
}
