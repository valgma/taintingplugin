package tainting.intention;

import com.intellij.psi.*;
import com.intellij.psi.util.PsiTreeUtil;
import com.siyeh.ig.psiutils.ExpressionUtils;
import com.siyeh.ipp.base.Intention;
import com.siyeh.ipp.base.PsiElementPredicate;
import org.jetbrains.annotations.Nls;
import org.jetbrains.annotations.NotNull;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

/**
 * Converts: res = st.executeQuery("SELECT * FROM  User WHERE userId='" + user + "'");
 *       To: PreparedStatement  preparedStatement=conn.prepareStatement("SELECT * FROM  User WHERE userId=?") ;
 *           preparedStatement.setString(1, user);
 *           ResultSet res = preparedStatement.executeQuery();
 *
 * First checks if the context is ok. Then creates the new statements.
 */
public class StatementToPreparedStatementIntention extends Intention {

    @Override
    protected void processIntention(@NotNull PsiElement element) {
        PsiPolyadicExpression expression = (PsiPolyadicExpression)element;
        PsiElement parent = expression.getParent();
        while (ExpressionUtils.isConcatenation(parent)) {
            expression = (PsiPolyadicExpression)parent;
            parent = expression.getParent();
        }

        // find statement...
        boolean canDoAutomaticRefactor = false;
        String connectionName = "";
        PsiMethodCallExpression executeCall = PsiTreeUtil.getParentOfType(element, PsiMethodCallExpression.class);
        PsiReferenceExpression methodRef = executeCall.getMethodExpression();

        if (methodRef.getText().endsWith("executeQuery")) {

            PsiElement statementRef = methodRef.getFirstChild();
            if (statementRef instanceof PsiReferenceExpression) {
                // assume we have following logic:
                //      Connection conn = ...
                //      Statement statement = conn.createStatement();
                //      statement.executeQuery(query)


                PsiElement statVarTmp = ((PsiReferenceExpression) statementRef).resolve();
                if (statVarTmp instanceof PsiVariable) {
                    PsiVariable statVar = (PsiVariable) statVarTmp;
                    PsiExpression statVarInitializer = statVar.getInitializer();
                    // we should have Connection.createStatement
                    if (statVarInitializer instanceof PsiMethodCallExpression) {
                        PsiReferenceExpression createStatRef = ((PsiMethodCallExpression) statVarInitializer).getMethodExpression();
                        if (createStatRef.getText().endsWith("createStatement")) {
                            PsiExpression connQualifier = createStatRef.getQualifierExpression();
                            if (connQualifier instanceof PsiReferenceExpression) {
                                PsiElement connVarTmp = ((PsiReferenceExpression) connQualifier).resolve();
                                if (connVarTmp instanceof PsiVariable) {
                                    PsiVariable connVar = (PsiVariable) connVarTmp;
                                    connectionName = connVar.getName();
                                    canDoAutomaticRefactor = true;
                                }
                            }
                        }
                    }
                }

            } else if (statementRef instanceof PsiMethodCallExpression) {
                // assume we have following:
                //      Connection conn = ...
                //      Connection.createStatement.executeQuery(query)
                PsiMethodCallExpression createStatementCall = (PsiMethodCallExpression) statementRef;
                PsiReferenceExpression createStatRef = createStatementCall.getMethodExpression();
                if (createStatRef.getText().endsWith("createStatement")) {
                    PsiExpression connQualifier = createStatRef.getQualifierExpression();
                    if (connQualifier instanceof PsiReferenceExpression) {
                        PsiElement connVarTmp = ((PsiReferenceExpression) connQualifier).resolve();
                        if (connVarTmp instanceof PsiVariable) {
                            PsiVariable connVar = (PsiVariable) connVarTmp;
                            connectionName = connVar.getName();
                            canDoAutomaticRefactor = true;
                        }
                    }
                }
            }
        }


        if (canDoAutomaticRefactor) {
            // first create the prepared query with placeholders. Extract this from string concatenation.
            final StringBuilder preparedQueryString = new StringBuilder("\"");
            List<PsiExpression> queryParameters = new ArrayList<>();

            PsiExpression[] operands = expression.getOperands();
            for (PsiExpression operand : operands) {
                if (operand instanceof PsiLiteralExpression) {
                    String txt = String.valueOf(((PsiLiteralExpression) operand).getValue());

                    // remove starting quote
                    txt = txt.trim();
                    if (txt.startsWith("'")) {
                        txt = txt.replaceFirst("'", "");
                    }
                    // remove ending quote
                    if (txt.endsWith("'")) {
                        txt = txt.substring(0, txt.length() - 1);
                    }

                    preparedQueryString.append(txt);
                } else {
                    preparedQueryString.append("?");
                    queryParameters.add(operand);
                }
            }
            preparedQueryString.append("\"");

            // make the prepared statement code and add it

            // create unique name for prep Statement
            String prepStatmentName = "preparedStatement";
            PsiMethod psiMethod = PsiTreeUtil.getParentOfType(expression, PsiMethod.class);
            List<String> methodLocalVars = PsiTreeUtil.findChildrenOfType(psiMethod, PsiLocalVariable.class).stream().map(x -> x.getName()).collect(Collectors.toList());
            int i_tmp = 0;
            while (methodLocalVars.contains(prepStatmentName)) {
                i_tmp++;
                prepStatmentName = "preparedStatement_" + i_tmp;
            }

            // create unique name for ResultSet variable
            String resultSetName = "resultSet";
            i_tmp = 0;
            while (methodLocalVars.contains(resultSetName)) {
                i_tmp++;
                resultSetName = "resultSet" + i_tmp;
            }

            // create element factory and find place to add new statement
            PsiElementFactory factory = JavaPsiFacade.getInstance(element.getProject()).getElementFactory();
            PsiElement anchor = PsiTreeUtil.getParentOfType(element, PsiStatement.class);
            PsiElement context = anchor.getContext();
            PsiElement env = anchor.getParent();

            // mark the fix range
            env.addAfter(factory.createCommentFromText( "/* --------END FIX----------- */", context), anchor);
            env.addBefore(factory.createCommentFromText("/* --------START FIX--------- */", context), anchor);

            // add new statements using PreparedStatement.setString() since in the original concat was valid
            PsiStatement statement = factory.createStatementFromText(String.format("PreparedStatement  %s=%s.prepareStatement(%s);", prepStatmentName, connectionName, preparedQueryString.toString()), context);
            anchor = env.addBefore(statement, anchor);

            for (int i = 0; i < queryParameters.size(); i++) {
                statement = factory.createStatementFromText(String.format("%s.setString(%d, %s);", prepStatmentName, i + 1, queryParameters.get(i).getText()) ,context);
                anchor = env.addAfter(statement, anchor);
            }

            statement = factory.createStatementFromText(String.format("ResultSet %s = %s.executeQuery();", resultSetName, prepStatmentName), context);
            env.addAfter(statement, anchor);

            // replace execute result in original statement
            PsiExpression resultSetExp = factory.createExpressionFromText(resultSetName, context);
            executeCall.replace(resultSetExp);
        }

    }

    /**
     * Returns when intention should be applicable.
     * Currently only when we have string concatenation.
     */
    @NotNull
    @Override
    protected PsiElementPredicate getElementPredicate() {
        return new PsiElementPredicate() {
            @Override
            public boolean satisfiedBy(PsiElement element) {
                return ExpressionUtils.isConcatenation(element);
            }
        };
    }

    @NotNull
    @Override
    public String getText() {
        return "Try to change Statement to PreparedStatement";
    }

    @Nls(capitalization = Nls.Capitalization.Sentence)
    @NotNull
    @Override
    public String getFamilyName() {
        return "Tainting fix";
    }


}
