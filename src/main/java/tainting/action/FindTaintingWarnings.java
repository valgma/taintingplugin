package tainting.action;

import com.intellij.codeInsight.daemon.DaemonCodeAnalyzer;
import com.intellij.compiler.CompilerConfigurationImpl;
import com.intellij.openapi.actionSystem.AnAction;
import com.intellij.openapi.actionSystem.AnActionEvent;
import com.intellij.openapi.actionSystem.CommonDataKeys;
import com.intellij.openapi.components.ServiceManager;
import com.intellij.openapi.module.Module;
import com.intellij.openapi.module.ModuleManager;
import com.intellij.openapi.project.Project;
import com.intellij.task.ProjectTaskManager;
import org.jetbrains.annotations.SystemIndependent;
import org.jetbrains.jps.model.java.compiler.ProcessorConfigProfile;
import org.jetbrains.jps.model.java.impl.compiler.ProcessorConfigProfileImpl;
import tainting.service.ObserveMessageWindow;

import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;

public class FindTaintingWarnings extends AnAction {
    /**
     * Set toolbar selection visible only if project is opened.
     */
    @Override
    public void update(AnActionEvent e) {
        final Project project = e.getData(CommonDataKeys.PROJECT);
        e.getPresentation().setVisible(project != null);
    }

    /**
     * Runs when action is invoked.
     * Adds Tainting checker as annotation processr. Bulds the project. Removes annotation processor.
     * NB! It uses currently environmental variables (CHECKFRAMEWORK) to find checker paths.
     */
    @Override
    public void actionPerformed(AnActionEvent e) {
        final Project project = e.getData(CommonDataKeys.PROJECT);
        assert project != null;
        ObserveMessageWindow observeMessageWindow = ServiceManager.getService(project, ObserveMessageWindow.class);

        // add annotation processor profile
        CompilerConfigurationImpl compilerConfigurationImpl = (CompilerConfigurationImpl) CompilerConfigurationImpl.getInstance(project);
        ProcessorConfigProfile myProfile = makeAnnotationProfile(project);
        compilerConfigurationImpl.addModuleProcessorProfile(myProfile);

        Path annotatedJDLPath = Paths.get(System.getenv("CHECKERFRAMEWORK"), "checker", "dist", "jdk8.jar");

        // stub file where new annotations can be added
        @SystemIndependent String basePath = e.getProject().getBasePath();
        Path stubPath = Paths.get(basePath, "main.astub");

        // add additional command line options to javac for each module
        for (Module module : ModuleManager.getInstance(project).getModules()) {
            List<String> additionalOptions = new ArrayList<>(compilerConfigurationImpl.getAdditionalOptions(module));
            additionalOptions.add("-Xbootclasspath/p:" + annotatedJDLPath);
            additionalOptions.add(String.format("-Astubs=%s", stubPath));

            compilerConfigurationImpl.setAdditionalOptions(module, additionalOptions);
        }


        // build the project and use callback to remove added processors and options
        ProjectTaskManager.getInstance(project).rebuildAllModules(executionResult -> {

            if (observeMessageWindow.isMessageWindowAvailable()) {
                observeMessageWindow.updateWarnings();

                // force the currently open file also get annotated
                DaemonCodeAnalyzer.getInstance(project).restart();

                // remove the annotator
                compilerConfigurationImpl.removeModuleProcessorProfile(myProfile);
                // remove the annotated jdk
                for (Module module : ModuleManager.getInstance(project).getModules()) {
                    List<String> additionalOptions = new ArrayList<>(compilerConfigurationImpl.getAdditionalOptions(module));
                    additionalOptions.remove("-Xbootclasspath/p:" + annotatedJDLPath);
                    additionalOptions.remove(String.format("-Astubs=%s", stubPath));
                    compilerConfigurationImpl.setAdditionalOptions(module, additionalOptions);
                }
            }
        });

    }

    /**
     * Makes new annotation profile (Settings->Build->Compiler->Annotation processors) and adds all modules to it.
     */
    private ProcessorConfigProfile makeAnnotationProfile(Project project) {
        ProcessorConfigProfile myProfile = new ProcessorConfigProfileImpl("");
        // TODO: CHECKERFRAMEWORK variable cannot contain other variables...like $HOME
        Path processrPath = Paths.get(System.getenv("CHECKERFRAMEWORK"), "checker", "dist", "checker.jar");
        myProfile.setProcessorPath(processrPath.toString());
        myProfile.addProcessor("org.checkerframework.checker.tainting.TaintingChecker");
        myProfile.setName("taintingAnnotator");
        myProfile.setObtainProcessorsFromClasspath(false);
        myProfile.setEnabled(true);

        for (Module module : ModuleManager.getInstance(project).getModules()) {
            myProfile.addModuleName(module.getName());
        }

        return myProfile;
    }
}
