package tainting.service;

import com.intellij.compiler.impl.CompilerErrorTreeView;
import com.intellij.ide.errorTreeView.ErrorTreeElement;
import com.intellij.ide.errorTreeView.ErrorViewStructure;
import com.intellij.ide.errorTreeView.GroupingElement;
import com.intellij.ide.errorTreeView.NavigatableMessageElement;
import com.intellij.openapi.fileEditor.OpenFileDescriptor;
import com.intellij.openapi.project.Project;
import com.intellij.openapi.vfs.VirtualFile;
import com.intellij.pom.Navigatable;
import com.intellij.psi.PsiElement;
import com.intellij.psi.PsiManager;
import com.intellij.ui.content.Content;
import com.intellij.ui.content.MessageView;
import org.jetbrains.annotations.NotNull;
import tainting.util.WarningData;

import java.util.*;
import java.util.stream.Collectors;

/**
 * This is a project service that can collect error message info from IntelliJ message window after building a project.
 * It collects all Tainting related errors and keeps a list of found errors for each file. Service can be queried for
 * current list of errors.
 */
public class ObserveMessageWindow {

    @NotNull private final Project project;
    private Map<String, List<WarningData>> warnings = new HashMap<>();  // list of problems for each file

    public ObserveMessageWindow(@NotNull Project project) {
        this.project = project;
    }

    public Map<String, List<WarningData>> getWarnings() {
//        updateWarnings();  // TODO: should warning be updated each time before querying?
        return warnings;
    }

    public boolean isMessageWindowAvailable() {
        MessageView messageView = MessageView.SERVICE.getInstance(project);
        Content content = messageView.getContentManager().getContent(0);
        return content != null;
    }

    public void updateWarnings() {
        warnings.clear();

        if (isMessageWindowAvailable()) {
            MessageView messageView = MessageView.SERVICE.getInstance(project);
            Content content = messageView.getContentManager().getContent(0);
            CompilerErrorTreeView listErrorView = (CompilerErrorTreeView) content.getComponent();
            ErrorViewStructure errorViewStructure = listErrorView.getErrorViewStructure();

            List<ErrorTreeElement> errorTreeElements = getErrorTreeElements(errorViewStructure);

            // For each warning, find corresponding PSI file and element
            PsiManager psiManager = PsiManager.getInstance(project);
            for (ErrorTreeElement errorTreeElement : errorTreeElements) {
                // these are already validated in other method
                Navigatable navigatable = ((NavigatableMessageElement) errorTreeElement).getNavigatable();
                OpenFileDescriptor openFileDescriptor = (OpenFileDescriptor) navigatable;

                VirtualFile file = openFileDescriptor.getFile();
                String fileCanonicalPath = file.getCanonicalPath();
                int offset = openFileDescriptor.getOffset();
                PsiElement psiElement = psiManager.findFile(file).findElementAt(offset);

                warnings.putIfAbsent(fileCanonicalPath, new ArrayList<>());
                List<WarningData> psiElements = warnings.get(fileCanonicalPath);
                psiElements.add(new WarningData(psiElement, errorTreeElement));
            }
        }
    }

    /**
     * Assume that when this is called, TaintingChecker has been used to compile the project/module/file and
     * some output is in the Message Window.
     * @return
     */
    private List<ErrorTreeElement> getErrorTreeElements(ErrorViewStructure errorViewStructure) {
        List<ErrorTreeElement> result = new ArrayList<>();

        Object errorViewStructureRootElement = errorViewStructure.getRootElement();
        ErrorTreeElement[] childElements = errorViewStructure.getChildElements(errorViewStructureRootElement);

        for (ErrorTreeElement childElement : childElements) {
            // Ignore simple messages, only file warning are relevant
            if (childElement instanceof GroupingElement) {
                GroupingElement groupingElement = (GroupingElement) childElement;

                // Now children should be individual warnings for that file
                ErrorTreeElement[] elements = errorViewStructure.getChildElements(groupingElement);
                for (ErrorTreeElement element : elements) {
                    if (isTaintingWarning(element)) {
                        if (element instanceof NavigatableMessageElement) {
                            result.add(element);
                        }
                    }
                }
            }
        }
        return result;
    }

    /**
     * Check if warning is produced by TaintingChecker. We can only have one type warnings:
     *  java: [assignment.type.incompatible] incompatible types in assignment.
     *  found: @Tainted
     *  required: @Untainted
     * The check is currently relaxed due to Windows formatting problems. It should still be adequate.
     */
    private boolean isTaintingWarning(ErrorTreeElement errorTreeElement) {
        String[] elementText = errorTreeElement.getText();
        String s = Arrays.stream(elementText).collect(Collectors.joining(" "));

        return s.contains("@Tainted") || s.contains("@Untainted");

//        if (elementText.length != 3) {
//            return false;
//        }
//
//        return Pattern.matches(".*\\[.*\\.type\\.incompatible].*", elementText[0]) &&
//                Pattern.matches(".*found.*:.*@Tainted.*", elementText[1]) &&
//                Pattern.matches(".*required.*:.*@Untainted.*", elementText[2]);
    }
}
