package tainting.util;

import com.intellij.ide.errorTreeView.ErrorTreeElement;
import com.intellij.psi.PsiElement;

import java.util.Arrays;
import java.util.stream.Collectors;

/**
 * Simple data class for keeping infor about single tainting error message.
 * Has the information about problematic element (PSI element) and also the whole error message.
 */
public class WarningData {
    private final PsiElement psiElement;
    private final ErrorTreeElement errorTreeElement;

    public WarningData(PsiElement psiElement, ErrorTreeElement errorTreeElement) {
        this.psiElement = psiElement;
        this.errorTreeElement = errorTreeElement;
    }

    public PsiElement getPsiElement() {
        return psiElement;
    }

    public ErrorTreeElement getErrorTreeElement() {
        return errorTreeElement;
    }

    public String getWarningText() {
        return Arrays.stream(errorTreeElement.getText()).collect(Collectors.joining("\n"));
    }
}
