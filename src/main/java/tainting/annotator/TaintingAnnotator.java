package tainting.annotator;

import com.intellij.lang.annotation.Annotation;
import com.intellij.lang.annotation.AnnotationHolder;
import com.intellij.lang.annotation.Annotator;
import com.intellij.openapi.components.ServiceManager;
import com.intellij.psi.PsiElement;
import com.intellij.psi.PsiExpression;
import com.intellij.psi.util.PsiTreeUtil;
import org.jetbrains.annotations.NotNull;
import tainting.intention.StatToPrepStatViaVar;
import tainting.intention.StatementToPreparedStatementIntention;
import tainting.intention.TaintingExpressionFixIntention;
import tainting.service.ObserveMessageWindow;
import tainting.util.WarningData;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * Annotator that checks highlights currently opened file. Each element is checked against list of problematic
 * expressions that were extracted from Checker Framework warnings.
 */
public class TaintingAnnotator implements Annotator {
    @Override
    public void annotate(@NotNull PsiElement element, @NotNull AnnotationHolder holder) {
        String canonicalPath = element.getContainingFile().getVirtualFile().getCanonicalPath();
        ObserveMessageWindow observeMessageWindow = ServiceManager.getService(element.getProject(), ObserveMessageWindow.class);
        Map<String, List<WarningData>> warnings = observeMessageWindow.getWarnings();

        if (warnings.containsKey(canonicalPath)) {
            List<WarningData> currentFileWarnings = warnings.getOrDefault(canonicalPath, new ArrayList<>());

            for (WarningData warningData : currentFileWarnings) {
                if (warningData.getPsiElement().equals(element)) {
                    // that element is too low level, need to extract the containing expression
                    PsiExpression psiExpression = PsiTreeUtil.getParentOfType(element, PsiExpression.class);

                    if (psiExpression != null) {
                        // create error annotation
                        Annotation annotation = holder.createErrorAnnotation(psiExpression, warningData.getWarningText());

                        // register possible quick fixes
                        annotation.registerFix(new StatementToPreparedStatementIntention());
                        annotation.registerFix(new StatToPrepStatViaVar(psiExpression));
                        annotation.registerFix(new TaintingExpressionFixIntention(psiExpression));
                    }
                }
            }
        }
    }
}
