# TaintingPlugin

This is a plugin for [IntelliJ IDEA](https://www.jetbrains.com/idea/) that uses [Checker Framework](https://checkerframework.org/) to preform interactive [tainting analysis](https://en.wikipedia.org/wiki/Taint_checking).  

## Setup
Everything needed to test the plugin is included in the repo.

1. Set up `CHECKERFRAMEWORK` environmental variable.  
	The Checker Framework distribution is under `/sampleCheckerFWProject/checker-framework-2.5.3`. Mark this directory as `CHECKERFRAMEWORK` variable value.  
2. Either import the project to IDE and run Gradle task `intelliJ/runIde` or run the task directly by `gradlew runIde`.  
	Project uses [Gradle task](https://www.jetbrains.org/intellij/sdk/docs/tutorials/build_system/prerequisites.html) `intellij/runIde` to start a new IntelliJ instance with the plugin enabled.  
3. Open the included `sampleCheckerFWProejct`
	It is probable that you need to specify new JDK. This can be done under `File/Project Structure/Project SDK`. Make sure you point it to Java 8 JDK!  
4. Choose from toolbar `TaintingMenu/Run Analysis`.  
	Now, if you open some files you should see problematic places are underlined. You can try to autofix them by invkoking `Alt + Enter` and choosing some option. Try to run analysis again.

New annotations can be added directly to source code or method signatures can be annoteted in the `main.astub` file inside project directory. Remember, you should annotate method inputs which require validation as `@Untainted`.


## Using plugin to analyse your project
The following is guide for Gradle projects. Otherwise you should follow Checker Framework [manual](https://checkerframework.org/manual/#creating-integrating-a-checker).

1. Make sure you have `CHECKERFRAMEWORK` environmental variable set up.
2. Add [`checkerframewor.gradle`](https://bitbucket.org/valgma/taintingplugin/src/master/sampleCheckerFWProject/checkerframework.gradle) to project folder.
3. Add `apply from: "checkerframework.gradle"' line to `build.gradle`
4. Import project to IntelliJ (NB! version that has plugin active).
5. Annotate code and run the analysis.
6. Add `main.astub` file to project folder and use that for library annotations.


## sampleCheckerFWProject
This project contains some examples of tainted flow vulnerabilities that can be used for plugin testing.

The included files:

* `TaintingExampleWithWarnings.java`  
    Includes a basic taint flow problem. Variables `input` and `input2` are tainted, methods `executeQuery, execute2, execute3` reguire untainted input. 
	Running the analysis will highlight problems. It should be solved by creating validation function, 
	template is created for user by using inention action (`Ctrl + Enter` on highlighted error).
* `SqlInjectionExample.java`  
    Example of an SQL injection vulnerability in `processRequestUnsafe` method. 
	Running tainting analysis should result in errors marked in the `INSECURE SECTION` and allowing user to apply auto fix.
* `AnnotatedMethods.java`  
    Displays example of dummy validation function with correct annotations (`validate`). 
	Also showcases that analysis is not limited to single class.
* `TrustboundExample.java`  
    Here, another information flow vulnerability is displayed. Session.putValue() is invoked with not validated 
	parameter. To detect it, putValue() method needs to annotated with @Untainted. It is done so in main.astub file. 
	Fix requires validating the param value (via validation method).
	Mehtod template can be create by using quick fix.
* `main.astub`  
    Annotations can be added here similar to examples. It can include several packages, 
	all classes under package name are assumed to be in that package. 
	Both user defined methods and library methods can be annotated.