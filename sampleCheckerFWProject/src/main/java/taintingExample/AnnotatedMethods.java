package taintingExample;

import org.checkerframework.checker.tainting.qual.Untainted;

public class AnnotatedMethods {

    // this is annotated in the main.astub file. However, annotating source coude is ok aswell.
    public static void executeQuery(String input) {
    }

    /**
     * Example validate method. Tainted input, untainted output and supressing of warnings.
     * User should implement the validation logic. But only annotations are needed for tainting checker to accept it.
     */
    @Untainted @SuppressWarnings("tainting")
    public static String validate(String userInput) {
        return userInput;
    }
}
