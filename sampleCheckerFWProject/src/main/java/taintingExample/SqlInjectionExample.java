package taintingExample;

// adjusted from https://www.javacodegeeks.com/2012/11/sql-injection-in-java-application.html

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;
import java.sql.*;

public class SqlInjectionExample {

    /**
     * Example of an SQL injection vulnerability. Running tainting analysis should result in errors marked in the
     * INSECURE SECTION and allowing user to apply auto fix.
     */
    protected void processRequestUnsafe(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        PrintWriter out = response.getWriter();
        try {
            Connection conn = null;
            String url = "jdbc:mysql://192.168.2.128:3306/";
            String dbName = "anvayaV2";
            String driver = "com.mysql.jdbc.Driver";
            String userName = "root";
            String password = "";
            try {
                Class.forName(driver).newInstance();
                conn = DriverManager.getConnection(url + dbName, userName, password);

                // ------------ INSECURE SECTION ----------------- //
                String user = request.getParameter("user");  // tainted source
                String passwd = request.getParameter("passwd");
                Statement st = conn.createStatement();
                String query = "SELECT * FROM  User where userId='" + user + "' AND passwd='" + passwd + "'";
                ResultSet res = st.executeQuery(query);  // untainted sink
                res = st.executeQuery("SELECT * FROM  User WHERE userId='" + user + "' AND passwd='" + passwd + "'");  // untainted sink
                ResultSet res2 = conn.createStatement().executeQuery("SELECT * FROM  User WHERE userId='" + user + "' AND passwd='" + passwd + "'");  // untainted sink
                // ----------------------------------------------- //

                System.out.println(query);
                out.println("Query : " + query);
                out.println("Results");
                while (res.next()) {
                    String s = res.getString("username");
                    out.println("\t\t" + s);
                }
                conn.close();

            } catch (Exception e) {
                e.printStackTrace();
            }
        } finally {
            out.close();
        }
    }

    /**
     * Here, the above problems are fixed.
     */
    protected void processRequestSafe(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        PrintWriter out = response.getWriter();
        try {
            Connection conn = null;
            String url = "jdbc:mysql://192.168.2.128:3306/";
            String dbName = "anvayaV2";
            String driver = "com.mysql.jdbc.Driver";
            String userName = "root";
            String password = "";
            try {
                Class.forName(driver).newInstance();
                conn = DriverManager.getConnection(url + dbName, userName, password);

                // ------------ THIS IS NOW SECURE SECTION ----------------- //
                String user = request.getParameter("user");  // tainted source
                PreparedStatement  preparedStatement=conn.prepareStatement("SELECT * FROM  User WHERE userId=?") ;
                preparedStatement.setString(1, user);  // perform validation
                ResultSet res = preparedStatement.executeQuery();  // untainted sink
                // ----------------------------------------------- //

                System.out.println(preparedStatement.toString());
                out.println("Query : " + preparedStatement.toString());
                out.println("Results");
                while (res.next()) {
                    String s = res.getString("username");
                    out.println("\t\t" + s);
                }
                conn.close();

            } catch (Exception e) {
                e.printStackTrace();
            }
        } finally {
            out.close();
        }
    }
}
