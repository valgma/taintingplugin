package taintingExample;

import org.checkerframework.checker.tainting.qual.Tainted;
import org.checkerframework.checker.tainting.qual.Untainted;


public class TaintingExampleWithWarnings {

    public static void main(String[] args) {
    }

    String getUserInput() {
        return "taintedStr";
    }

    void processRequest() {
        String input = getUserInput();
        String input2 = getUserInput();
        AnnotatedMethods.executeQuery(input);  // tainting error
//        execute2(input, input2);
//        execute3(input, input2);
    }

    /**
     * both input parameters are required to be untainted
     */
    public void execute2(@Untainted String in1, @Untainted String in2) {
        String tmp = in1 + in2;
    }

    /**
     * only one input parameter needs to be untainted
     */
    public void execute3(String in1, @Untainted String in2) {
        String tmp = in1 + in2;
    }

    /**
     * Example of local inference. The following method is correct
     * since result variable will be inferred to be untainted.
     */
    @Untainted
    public String inference(@Untainted String bar) {
        @Tainted String result = bar;
        return result;
    }
}