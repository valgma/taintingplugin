// adjusted from OWASP benchmarks
// https://www.owasp.org/index.php/Benchmark

package taintingExample;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@WebServlet(value="/trustbound-00/BenchmarkTest00031")
public class TrustboundExample extends HttpServlet {

    private static final long serialVersionUID = 1L;

    @Override
    public void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        doPost(request, response);
    }

    /**
     * Here, another information flow vulnerability is displayed. Session.putValue() is invoked with not validated
     * parameter. To detect it, putValue() method needs to annotated with @Untainted. It is done so in main.astub file.
     * Fix requires validating the param value (via validation method).
     * Mehtod template can be create by using quick fix.
     */
    @Override
    public void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        // some code
        response.setContentType("text/html;charset=UTF-8");

        java.util.Map<String, String[]> map = request.getParameterMap();
        String param = "";
        if (!map.isEmpty()) {
            String[] values = map.get("BenchmarkTest00031");
            if (values != null) param = values[0];
        }

        // javax.servlet.http.HttpSession.putValue(java.lang.String,java.lang.Object^)
        request.getSession().putValue( "userid", param);
    }

}
